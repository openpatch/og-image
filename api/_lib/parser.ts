import { IncomingMessage } from 'http';
import { parse } from 'url';
import { ParsedRequest } from './types';

export function parseRequest(req: IncomingMessage) {
    console.log('HTTP ' + req.url);
    const { pathname, query } = parse(req.url || '/', true);
    const { username, type, theme, avatar } = (query || {});

    if (Array.isArray(username)) {
        throw new Error('Expected a single username');
    }
    if (Array.isArray(type)) {
        throw new Error('Expected a single type');
    }
    if (Array.isArray(theme)) {
        throw new Error('Expected a single theme');
    }
    if (Array.isArray(avatar)) {
        throw new Error('Expected a single avatar');
    }
    
    const arr = (pathname || '/').slice(1).split('.');
    let extension = '';
    let text = '';
    if (arr.length === 0) {
        text = '';
    } else if (arr.length === 1) {
        text = arr[0];
    } else {
        extension = arr.pop() as string;
        text = arr.join('.');
    }

    const parsedRequest: ParsedRequest = {
        fileType: extension === 'jpeg' ? extension : 'png',
        text: decodeURIComponent(text),
        type: type || "Item",
        username: username || "dummy-user",
        avatar: avatar,
        theme: theme === 'dark' ? 'dark' : 'light'
    };
    return parsedRequest;
}
