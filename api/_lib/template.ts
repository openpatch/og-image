
import { readFileSync } from 'fs';
import { sanitizeHtml } from './sanitizer';
import { ParsedRequest } from './types';

const rglr = readFileSync(`${__dirname}/../_fonts/Inter-Regular.woff2`).toString('base64');
const bold = readFileSync(`${__dirname}/../_fonts/Inter-Bold.woff2`).toString('base64');
const mono = readFileSync(`${__dirname}/../_fonts/Vera-Mono.woff2`).toString('base64');
const logo = readFileSync(`${__dirname}/../_images/logo.png`).toString('base64');
const defaultAvatar = readFileSync(`${__dirname}/../_images/default_avatar.png`).toString('base64');

function getCss(theme: string) {
    let primary = "#006f95";
    let secondary = "#98ff98";
    let background = 'white';
    let foreground = primary;
    let radial = secondary;

    if (theme === 'dark') {
        background = "#303030";
        foreground = 'white';
        radial = secondary;
    }
    return `
    @font-face {
        font-family: 'Inter';
        font-style:  normal;
        font-weight: normal;
        src: url(data:font/woff2;charset=utf-8;base64,${rglr}) format('woff2');
    }

    @font-face {
        font-family: 'Inter';
        font-style:  normal;
        font-weight: bold;
        src: url(data:font/woff2;charset=utf-8;base64,${bold}) format('woff2');
    }

    @font-face {
        font-family: 'Vera';
        font-style: normal;
        font-weight: normal;
        src: url(data:font/woff2;charset=utf-8;base64,${mono})  format("woff2");
      }

    body {
        background: ${primary};
        background-image: radial-gradient(circle at 25px 25px, ${radial} 2%, transparent 0%), radial-gradient(circle at 75px 75px, ${radial} 2%, transparent 0%);
        background-size: 100px 100px;
        height: 100vh;
        width: 100vw;
        display: flex;
        text-align: center;
        align-items: center;
        justify-content: center;
    }

    code {
        color: #D400FF;
        font-family: 'Vera';
        white-space: pre-wrap;
        letter-spacing: -5px;
    }

    code:before, code:after {
        content: '\`';
    }

    .spacer {
        margin: 150px;
    }

    .emoji {
        height: 1em;
        width: 1em;
        margin: 0 .05em 0 .1em;
        vertical-align: -0.1em;
    }
    
    .heading {
        font-family: 'Inter', sans-serif;
        font-size: 150px;
        text-overflow: ellipsis;
        overflow: hidden;
        font-style: normal;
        font-weight: bold;
        color: ${foreground};
        line-height: 1.2;
        text-align: center;
        max-height: 350px;
        vertical-align: middle;
    }

    .card {
      position: relative;
      display: flex;
      height: 60vh;
      width: 85vw;
      background: ${background};
      border: 2px solid ${foreground};
      padding: 50px;
      text-align: center;
      align-items: center;
      justify-content: center;
    }

    .logo {
        position: absolute;
        right: 50px;
        bottom: 50px;
        width: 120px;
        height: auto;
    }

    .type {
        position: absolute;
        top: 50px;
        left: 50px;
        text-align: left;
        color: ${foreground};
        font-family: 'Inter', sans-serif;
        font-size: 60px;
        font-weight: bold;
    }

    .avatar {
        position: relative;
        overflow: hidden;
        width: 120px;
        height: 120px;
        text-align: center;
        border-radius: 50%;
        margin-right: 30px;
    }

    .avatar img {
      width: 100%;
      height: auto;
    }

    .username {
        position: absolute;
        display: flex;
        left: 50px;
        bottom: 50px;
        font-family: 'Inter', sans-serif;
        font-size: 60px;
        font-style: normal;
        text-align: center;
        align-items: center;
        justify-content: center;
        color: ${foreground};
    }`;
}

export function getHtml(parsedReq: ParsedRequest) {
    const { text, theme, type, username, avatar } = parsedReq;

    return `<!DOCTYPE html>
<html>
    <meta charset="utf-8">
    <title>Generated Image</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        ${getCss(theme)}
    </style>
    <body>
        <div class="card">
            <div class="heading">${text}</div>
            <div class="type">${type}</div>
            <img class="logo" alt="Generated Image" src="data:image/png;base64, ${logo}" />
            <div class="username">
              <div class="avatar">
                <img alt="Avatar" src="${avatar ? sanitizeHtml(avatar) : `data:image/png;base64, ${defaultAvatar}`}" />
              </div>
              <span>${username}</span>
            </div>
        </div>
    </body>
</html>`;
}
