export type FileType = 'png' | 'jpeg';
export type Theme = 'light' | 'dark';

export interface ParsedRequest {
    fileType: FileType;
    text: string;
    type: string;
    username: string;
    avatar: string;
    theme: Theme;
}
